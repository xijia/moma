﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class MotionMatcher : MonoBehaviour
{
    //CalculateCost calculateCost = new CalculateCost();
    public AnimationCapsules animationCapsules;
    public CapsuleScriptObject current;
    public Result result;
    public Transform PlayerTransform;
    public AnimClip animTest;
    public int Kneibor = 100;
    public int Difference=60;

    private Dictionary<string, Transform> _skeletonJoints = new Dictionary<string, Transform>();

    // Start is called before the first frame update
    void Start()
    {
        result.CapsuleNum = 0;
        transf(transform.transform);
        GetAllChildren(transform.transform);
    }

    private void GetAllChildren(Transform trans)
    {
        //SkeletonJoints.Add("Root", trans);
        foreach (Transform child in trans)
        {
            if (child.childCount > 0) GetAllChildren(child);
            _skeletonJoints.Add(child.name, child);
        }
    }
    // Update is called once per frame
    void Update()
    {
        var BestFrameIndex = GetBestFrameIndex(animationCapsules, current.Capsule);//calculateCost.
        

        var bestFrame = animationCapsules.FrameCapsules[BestFrameIndex];
        bool isSameLocation = (BestFrameIndex == result.CapsuleNum);
                                //|| ((bestFrame.AnimClipName == result.ClipName)
                                //&& (Mathf.Abs(bestFrame.FrameNum - result.FrameNum) < 11));

        //Debug.Log(bestFrame.AnimClipName);
        //Debug.Log(bestFrame.FrameNum);
        if (!isSameLocation)
        {
            result.ClipName = bestFrame.AnimClipName;
            result.FrameNum = bestFrame.FrameNum;
            result.CapsuleNum = BestFrameIndex;
        }
        else
        {
            
            result.CapsuleNum++;
            
            result.FrameNum = animationCapsules.FrameCapsules[result.CapsuleNum].FrameNum;
            current.Capsule.FrameNum = result.FrameNum;
        }
        PlayAnimationJoints(result, _skeletonJoints);

    }

    public void PlayAnimationJoints(Result result,
                                    Dictionary<string, Transform> skeletonJoints)

    {

        //current.Capsule.AnimClipIndex = result.AnimClipIndex;
        //current.Capsule.AnimClipName = result.ClipName;
        //current.Capsule.FrameNum = result.FrameNum;
        //current.Capsule.CapsuleIndex = result.CapsuleNum;

        //if (result.FrameNum >= animationClips.AnimClips[result.AnimClipIndex].Frames.Count - 1)
        //    result.FrameNum = 0;


        FrameToJoints(skeletonJoints,
                       animTest.Frames[result.FrameNum]);
        //transform.Rotate(rotationPlayer);
    }

    private void transf(Transform trans)
    {
        foreach (Transform child in trans)
        {
            if (child.childCount > 0) transf(child);
            if (child.name.Contains("mixamorig:"))
            {
                child.name = child.name.Replace("mixamorig:", "");
            }
        }
    }

    public void FrameToJoints(
                              Dictionary<string, Transform> skeletonJoints,
                              AnimationFrame frame)
    {
        //Quaternion rotation = Quaternion.identity;
        //Vector3 position = Vector3.zero;

        foreach (var jointPoint in frame.JointPoints)
        {
            if (!skeletonJoints.Keys.Contains(jointPoint.Name))
            {
                continue;
            }


            var joint = skeletonJoints[jointPoint.Name];
            ApplyJointPointToJoint(jointPoint, joint);
        }

        //transform.Rotate(rotationEular);
    }

    private void ApplyJointPointToJoint(AnimationJointPoint jointPoint, Transform joint)
    {
        joint.rotation = transform.rotation * jointPoint.Rotation;
        //joint.rotation = jointPoint.Rotation;
        joint.position = transform.TransformDirection(jointPoint.Position) + transform.position;
        //joint.position = jointPoint.Position + transform.position;
    }



    public int GetBestFrameIndex(AnimationCapsules animationCapsules, Capsule current)
    {

        float bestScore = float.MaxValue;
        int bestIndex = 0;
        var bestTrajs = FindBestTrajectories(animationCapsules, current);
        result.allBestTraj = bestTrajs;
        for(int i = 0; i< bestTrajs.Count; i++)
        {
            var index = bestTrajs[i];
            var poseCost = PoseCost(animationCapsules.FrameCapsules[index], current);

            if (poseCost < bestScore)
            {
                //avoid backwards
                //if ((index < result.FrameNum) && (Mathf.Abs(index - result.FrameNum) < 20))
                //    continue;

                bestScore = poseCost;
                bestIndex = index;
            }
        }

        return bestIndex;
    }


    public int GetBestFrameIndex_Original(AnimationCapsules animationCapsules, Capsule current)
    {
        int BestIndex = 0;
        float bestScore = float.MaxValue;


        for (int i = 0; i < animationCapsules.FrameCapsules.Count; i++)
        {
            //for debug


            var score = TrajectoryCost(animationCapsules.FrameCapsules[i], current)
                 + PoseCost(animationCapsules.FrameCapsules[i], current);


            //if (i == 386)
            //    Debug.Log(score);

            if (bestScore > score)
            {
                bestScore = score;
                BestIndex = i;
            }
        }

        return BestIndex;
    }


    private List<int> FindBestTrajectories(AnimationCapsules animationCapsules,
                                            Capsule current)
    {
        //决定了终止的尽头
        int bestNum = Kneibor;//k
        //float scoreMax = float.MaxValue;
        List<float> scores = new List<float>();
        List<int> frameindex = new List<int>();
        //initialize
        for (int i = 0; i < bestNum; i++)
        {
            scores.Add(float.MaxValue);
            frameindex.Add(0);
        }

        for (int i = 0; i < animationCapsules.FrameCapsules.Count; i++)
        {

            var score = TrajectoryCost(animationCapsules.FrameCapsules[i], current);

            //used linq
            if (scores.Max() > score)
            {
                ////avoid backwards
                var frameNum = animationCapsules.FrameCapsules[i].FrameNum;
                if (frameNum < result.FrameNum && Mathf.Abs(frameNum - result.FrameNum) < Difference)
                    continue;

                var maxIndex = scores.IndexOf(scores.Max());
                scores[maxIndex] = score;
                frameindex[maxIndex] = i;
               
            }
        }


        //normalized
        //var minScore = scores.Min();
        //var gapScore = scores.Max() - minScore;
        //for (int i = 0; i < bestNum; i++)
        //    scores[i] = (scores[i] - minScore) / gapScore;

        //scoreWithIndex.capsuleIndex = frameindex;
        //scoreWithIndex.scores = scores;

        return frameindex;
    }

    private float TrajectoryCost(Capsule frame, Capsule current)
    {
        float trajectoryCost = 0;
        //assume future length == history
        for (int i = 0; i < current.TrajectoryFuture.Length; i++)
        {
            var futurePos = (PlayerTransform.TransformDirection(frame.TrajectoryFuture[i]) - current.TrajectoryFuture[i]).magnitude;
            var historyPos = (PlayerTransform.TransformDirection(frame.TrajectoryHistory[i]) - current.TrajectoryHistory[i]).magnitude;
            trajectoryCost += (futurePos + historyPos);
        }

        return trajectoryCost;
    }

    private float PoseCost(Capsule frame, Capsule current)
    {
        float poseCost = 0;

        for(int i = 0; i< current.KeyJoints.Count; i++)
        {
            poseCost = (frame.KeyJoints[i].Position - current.KeyJoints[i].Position).magnitude;
        }

        return poseCost;
    }
}
