﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Result : ScriptableObject
{
    //for debug
    public string ClipName;
    public int FrameNum;


    public int CapsuleNum;
    //for debug
    public List<int> allBestTraj;
}
